# SOCKET S1 - S2 - S3


## Apps 

```

#info
It is recommended to turn on the apps in the following order S3 - S2 - S1, the instructions to deploy each service are in the redmine.md of each one and then operate from s1

all the tests are set up to interact with the product of id 1 hosted in mongodb

# S1 app
socket client in python

This app has a small menu where we can choose the monitor option which checks the prices of a specific product for a period of 20 seconds at intervals of 5 seconds per request.

Should be tested as development with the dev start command

While this app waits for requests, it updates the price of a specific product at time intervals.

# S2 app
server socket in javascript
this application maintains a socket connection with s1 while requesting s3 for updated information

# S3 app
api rest in express nodejs
this app uses mongo as its database and connects to an instance of it at https://cloud.mongodb.com/

```
