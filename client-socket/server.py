import socket
import os
import threading
import time

def close_socket(client_socket:dict) -> None :
    # Cerrar el socket del cliente
    client_socket.close()
    print("")
    print("Socket cerrado ")

def connect_socket(client_socket:dict) -> None :    
    # Conectar al servidor remoto
    client_socket['client'].connect((client_socket['ip'], client_socket['port']))
    print("")
    print("Socket abierto ")

def clear_console() -> None :
    if os.name == "posix":
        os.system("clear")
    elif os.name == "ce" or os.name == "nt" or os.name == "dos":
        os.system("cls")

def validate_opcion(print_menu ,limit_num:int,config_print={}) -> int :
    '''
    Pre:  Recibe una funcion que imprime en pantalla y sus parametros si necesita, numero limite de opciones.
    Post: Devuelve un valor entero entre el rango dado por el numero limite, 
    Sino  vuelve a pedir otra opcion. 
    '''
    if config_print:
        print_menu(config_print)
    else:
        print_menu()
    opcion = input("Ingrese una opción: ")
    
    while not opcion.isnumeric() or not (0 < int(opcion) <= limit_num) :
        if config_print:
            print_menu(config_print)
        else:
            print_menu()        
        print("La opción ingresada, no es valida")
        opcion = input("Ingrese una opción: ")
    return int(opcion)  

def print_menu_principal() -> None :
    clear_console()
    print("Products")
    print("1. monitorear precio")
    print("2. Salir")

def get_price(client_socket:dict) -> None :
    # Enviar datos al servidor
    datos = "1"
    client_socket.send(datos.encode())

    # Recibir datos del servidor
    get_data = client_socket.recv(1024)
    print("Server - ", get_data.decode())

def timer(timer_runs,client):
    while timer_runs.is_set():
        print("")
        get_price(client)
        time.sleep(5)   # 5 segundos.

def monitor(server_info:dict):
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_info['client'] = client_socket        
    connect_socket(server_info)

    # genera un loop para pedir informacion a S2
    timer_runs = threading.Event()
    timer_runs.set()
    t = threading.Thread(target=timer, args=(timer_runs,server_info['client']))
    t.start()

    # Espera 20 segundos y luego detener el timer.
    time.sleep(20)
    timer_runs.clear()
    print("")
    input("presiona para continuar")
    close_socket(server_info['client'])
           

def menu(server_info:dict) -> None:
    program = True
    while program: 
        opcion = validate_opcion(print_menu_principal,2)
        if   opcion == 1:
            monitor(server_info)     
        elif opcion == 2:
            program = False
        else:
            print("La opción ingresada, no es valida")  

def main() -> None:
        server_info = {"ip":"127.0.0.1","port":4000}
        menu(server_info)

main()
 
