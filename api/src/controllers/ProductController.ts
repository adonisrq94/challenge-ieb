import { Request, Response } from 'express';

import ProductsService from '../services/products';



class ProductController {


    constructor() {

    }

    

    public async getProducts(req: Request, res: Response ): Promise<void>  {
        try {
            const response = await ProductsService.getProducts()
            if (response.length > 0) {
                res.status(200).json({
                    data: response,
                    message: 'list users'
                });
            } else {
                res.status(404).json({
                    message: 'not found'
                });
            }

        } catch (error) {
            res.status(500).json({
                error: error,
            });
        }
    }

    async getProduct(req: Request, res: Response ): Promise<void> {
        try {
            const product = await ProductsService.getProduct(req.params.id);
            if (product.length > 0) {
                res.status(200).json({
                    data: product,
                    message: 'get product'
                });
            } else {
                res.status(404).json({
                    message: 'not found'
                });
            }
        } catch (error) {
            res.status(500).json({
                error: error,
            });
        }
    }

    async createProduct(req: Request, res: Response ): Promise<void> {
        try {
            const newUser = await ProductsService.createProduct(req.body);
            res.status(201).json({
                data: newUser,
                message: 'product  created'
            });
        } catch (error) {
            res.status(500).json({
                error: error,
            });        
        }
    }


    public async updateProduct(id:string): Promise<void> {
        try {
            const product = await ProductsService.getProduct(id);
            if (product.length > 0) {
                let item = product[0]
                if (item.price > 1500) {
                    item.price = 1000
                    item.listPrice = 800
                } else {
                    item.price = 2000
                    item.listPrice = 1800
                }
                    
                const productUpdate = await ProductsService.updateProduct(item._id, item);
                console.log("")
                console.log("Price update")
                console.log("")
            } else {
                console.log("not found")
            }
        } catch (error) {
            console.log("error",error)

        }
    }

}

const controller = new ProductController()

export default controller;


