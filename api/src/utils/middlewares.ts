import { Request, Response } from 'express';


const notFound = (req: Request, res: Response ):void => {
    res.status(404);
    res.send({ 'Error':"Not Found" })
};



export default notFound ;