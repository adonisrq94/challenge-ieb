import { Schema, model, Document } from "mongoose";

const ProductSchema = new Schema({
  id: { type: String, required: true, unique: true },
  description: { type: String, required: true },
  price: { type: Number, required: true },
  listPrice: { type: Number, required: true },
});


export interface IProduct extends Document{
  id:string;
  description: string;
  price: number;
  listPrice: number;
}

export default model<IProduct>("Product", ProductSchema);
