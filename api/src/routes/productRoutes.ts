import { Router } from 'express';

import ProductController from '../controllers/ProductController';


class ProductRouter {
    public router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    routes(): void {
        this.router.get('/', ProductController.getProducts);
        this.router.get('/:id', ProductController.getProduct);
        this.router.post('/', ProductController.createProduct);
    }

}

const productRouter = new ProductRouter();
export default productRouter.router;

