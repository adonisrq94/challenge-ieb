
import Product, { IProduct } from '../models/Product';

class ProductsService {

    constructor() {}

    async getProducts():Promise<any>{
        const product = await Product.find();
        return product;
    }

    async getProduct(id: string): Promise<any> {
        const product = await Product.find({ id: id});
        return product;
    }
    
    async updateProduct(id:any, data:any): Promise<any> {
        const product = await Product.findByIdAndUpdate(id, data, {new: true});
        return product;
    }

    async createProduct(data: any): Promise<any> {
        const newProduct: IProduct = new Product(data);
        await newProduct.save();   
        return newProduct;        
    }

}

const service = new ProductsService()

export default service;

