import express from 'express';
import morgan from 'morgan';
import helmet from 'helmet';
import compression from 'compression';
import mongoConnect from './databases';
import notFound  from './utils/middlewares';
import cors from 'cors';


// import routes
import indexRoutes from './routes/indexRoutes';
import ProductRoutes from './routes/productRoutes';


class Server {
    public app: express.Application;

    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }

    public config(): void {
        // Settings
        this.app.set('port', process.env.PORT || 3000);
        mongoConnect.start()
        // middlewares
        this.app.use(morgan('tiny'));
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended: false}));
        this.app.use(helmet());
        this.app.use(compression());
        this.app.use(cors());
    }

    public routes(): void {
        const router: express.Router = express.Router();

        this.app.use('/', indexRoutes);
        this.app.use('/api/products', ProductRoutes);
        this.app.use(notFound)
    }

    public start(): void {
        this.app.listen(this.app.get('port'), () => {
            console.log('Server on port', this.app.get('port'));
        });
    }
}

export { Server } ;