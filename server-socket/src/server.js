const net = require('net');
const axios = require('axios');


const server = net.createServer()
const URL = "http://localhost:3000/api/products";


// peticion de informacion al api S3
const getProduct = (id,socket) => {
    axios.get(`${URL}/${id}`)
      .then(res => {
        let data = res.data.data[0]
        socket.write(`id: ${data.id} / price: ${data.price} / price venta: ${data.listPrice} / descripcion: ${data.description}`)

      }).catch(function(error) {
        if (error.response) {
          console.log(error.response)
          socket.write('ERROR!')
        } 
    })
}


server.on('connection', (socket)=>{
    socket.on('data', (data)=>{
        console.log('\nclient' + socket.remoteAddress + ":" + socket.remotePort + " id: " + data)
        getProduct("1",socket)
    })

    socket.on('close', ()=>{
        console.log('close connection')
    })

    socket.on('error', (err)=>{
        console.log(err.message)
    })
})

server.listen(4000, ()=>{
    console.log('server on port ', server.address().port)
})

